/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.casas;

import java.util.Scanner;

/**
 *
 * @author YordyHz
 */
public class Main {

    public static void main(String[] args) {
        int n = 8;
        int casas[] = new int[n];
        int resultado[] = new int[n];
        int dias, in, temp;
        Scanner ent = new Scanner(System.in);

        try {
            System.out.println("Ingrese las " + n + " entradas: ");
            for (int i = 0; i < casas.length; i++) {
                in = ent.nextInt();
                if (in != 0 && in != 1) {
                    System.out.println("Solo se permiten los valores 1 y 0. \nIntente nuevamente: ");
                    i--;
                } else {
                    casas[i] = in;
                }
            }
            System.out.println("Ingrese los días: ");
            dias = ent.nextInt();

            System.out.println("\nCantidad de días: " + dias);
            entrada(casas);

            for (int i = 0; i < dias; i++) {
                for (int j = 0; j < casas.length; j++) {
                    if (j == 0) {
                        temp = nuevoValor(0, casas[j + 1]);
                    } else if (j == casas.length - 1) {
                        temp = nuevoValor(casas[j - 1], 0);
                    } else {
                        temp = nuevoValor(casas[j - 1], casas[j + 1]);
                    }
                    resultado[j] = temp;
                }
                casas = resultado.clone();
                salida(i + 1, casas);
            }


        } catch (Exception ex) {
            System.out.println("Ha ocurrido un error.");
            System.out.println(ex);
        }

    }

    static void salida(int x, int[] a) {
        System.out.println("\nSalida día " + x + ": ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]);
        }
    }

    static void entrada(int[] a) {
        System.out.println("Entrada: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]);
        }
    }

    static int nuevoValor(int a, int b) {
        return (a == b) ? 0 : 1;
    }

}
